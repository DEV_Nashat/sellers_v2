<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CommonQuestions extends Component
{
    public function render()
    {
        return view('livewire.common-questions');
    }
}
