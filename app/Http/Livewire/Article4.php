<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Article4 extends Component
{
    public function render()
    {
        return view('livewire.article4');
    }
}
