<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Article2 extends Component
{
    public function render()
    {
        return view('livewire.article2');
    }
}
