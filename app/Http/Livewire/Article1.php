<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Article1 extends Component
{
    public function render()
    {
        return view('livewire.article1');
    }
}
