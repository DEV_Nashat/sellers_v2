<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Article3 extends Component
{
    public function render()
    {
        return view('livewire.article3');
    }
}
