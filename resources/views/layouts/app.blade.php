<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{asset('storage/img/favicon.ico')}}">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="{{asset('css/blog.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/knowledge.css')}}">
    <link rel="stylesheet" href="{{asset('css/fontello.css')}}" > 
    <script src= "{{asset('js/jquery-3.3.1.min.js')}}"></script> 
    <script src="{{asset('js/knowledge.js')}}"></script>
    <script src="{{asset('js/frontend.js')}}"></script>
    <title>Bazzarry - Seller</title>

    <livewire:styles />
    <livewire:scripts />

</head>
<body> 
   
   {{$slot}}

</body>
</html>