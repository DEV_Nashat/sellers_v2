<div style="width:100%;min-height:100vh;display:flex;flex-flow:column;justify-content:space-between">
    <header class="main-header">
    <nav class="main-nav">
        <a href="/sellers/blog">
            <img src="../views/img/logo-black.png" alt="Bazzarry logo" class="menu-logo">
        </a>
        <ul class="nav-menu">
            <li>
                <a href="/sellers/blog">أحدث المقالات</a>
            </li>
        </ul>
    </nav>
    </header>
    <section class="article-container">
        <div class="article-top">
            <h1>كيفية التواصل مع مركز بائعي بازاري</h1>
        </div>
        <article class="article-text">
            <p>
                في حال أردت التأكد من السياسات والأحكام الخاصة بالبائعين أو واجهتك أي صعوبة في إحدى العمليات يمكنك التواصل مع مدير صفحتك( Adviser) وتقديم استفساراتك بكل سهولة وأريحية وسيقدم هو بدوره كل النصائح  التي من شأنها مساعدتك على إكمال جميع الخطوات والعمليات وسيكون دائماً على أتم الاستعداد لتوجيهك ومساندتك كلما احتجت للمساعدة.
            </p>
            <p>
                أرقام المندوبين :
            </p>
            <p>
                عبر الايميل :
            </p>
        </article>
        <section class="article-tags-container">
            <p>علامات هذه التدوينة</p>
            <div class="tags-box">
                <a href="#" class="tags">التواصل</a>
            </div>
        </section>
        <footer class="article-footer">
            <p>هل كان هذا المقال مفيدًا</p>
            <div>
                <button type="button" class="article-useful-btn">
                    <i class="icon-emo-happy"></i>
                    <span>نعم</span>
                </button>
                <button type="button" class="article-unuseful-btn">
                    <i class="icon-emo-unhappy"></i>
                    <span>لا</span>
                </button>
            </div>
        </footer>
    </section>
    <footer class="main-footer">
    <section class="footer-right">
        <img src="../views/img/footer-logo.jpg" alt="Bazzarry logo" class="bazzarry-logo">
    </section>
    <section class="footer-links">
        <h3 class="footer-section-title">القوانين والأحكام</h3>
        <a href="#" class="footer-pages">سياسات بازاري</a>
    </section>
    <section class="footer-social">
        <h3 class="footer-section-title">وسائل التواصل</h3>
        <a href="https://www.bazzarry.com/" target="_blank" class="footer-social-link">
            <span>bazzarry.com</span>
            <i class="icon-globe"></i>
        </a>
        <a href="#" target="_blank" class="footer-social-link">
            <span>8000501</span>
            <i class="icon-phone"></i>
        </a>
        <a href="https://api.whatsapp.com/send?phone=967776600900&text=Source%3A%20Seller%20page" target="_blank" class="footer-social-link">
            <span>776600900</span>
            <i class="icon-whatsapp"></i>
        </a>
        <a href="https://www.facebook.com/bazzarry" target="_blank" class="footer-social-link">
            <span>bazzarry@</span>
            <i class="icon-facebook"></i>
        </a>
        <a href="https://twitter.com/bazzarry" target="_blank" class="footer-social-link">
            <span>bazzarry@</span>
            <i class="icon-twitter"></i>
        </a>
        <a href="https://www.youtube.com/channel/UCxyV4TB07GeCqB84l1QlrNg" target="_blank" class="footer-social-link">
            <span>bazzarry@</span>
            <i class="icon-youtube"></i>
        </a>
    </section>
    </footer>
</div>