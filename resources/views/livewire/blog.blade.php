
<div class="blog" style="width:100%;min-height:100vh;display:flex;flex-flow:column;justify-content:space-between">
     <header class="main-header">
        <nav class="blog-main-nav">
            <a href="#">
                <img src="{{asset('storage/img/logo-black.svg')}}" alt="Bazzarry logo" class="menu-logo">
            </a>
            <ul class="blog-nav-menu">
                <li>
                    <a href="#">أحدث المقالات</a>
                </li>
            </ul>
        </nav>
     </header>
     <section class="latest-news" id="main-latest-news">
         <div class="title-box">
            <h3 class="section-title">أحدث المقالات</h3>
            <div class="title-line"></div>
         </div>
        <div class="news-cards">
            <div class="card">
                <div class="card-news-thumb">
                    <h4 class="card-news-title">كيفية إدارة الحساب في بازاري</h4>
                    <p class="card-qoute"> 
                      تستطيع إدارة حسابك من خلال الحساب الخاص بك على منصة البائعين 
                    </p>
                </div>
                <a href="{{route('/blog/1')}}" class="card-read-more">اقرأ المزيد</a>
            </div>
            <div class="card">
                <div class="card-news-thumb">
                    <h4 class="card-news-title">آلية دفع المستحقات</h4>
                    <p class="card-qoute"> 
                      إن عملية دفع المستحقات تتم بشكل سلس ومنظم عن طريق إيداع المبلغ 
                    </p>
                </div>
                <a href="{{route('/blog/2')}}" class="card-read-more">اقرأ المزيد</a>
            </div>
            <div class="card">
                <div class="card-news-thumb">
                    <h4 class="card-news-title">طريقة إدارة محتوى المنتج </h4>
                    <p class="card-qoute"> 
                      يوفر لك بازاري دليل تفصيلي واضح ومحدد يحوي مجموعة 
                    </p>
                </div>
                <a href="{{route('/blog/3')}}" class="card-read-more">اقرأ المزيد</a>
            </div>
            <div class="card">
                <div class="card-news-thumb">
                    <h4 class="card-news-title">كيفية التواصل مع مركز بائعي بازاري</h4>
                    <p class="card-qoute"> 
                      في حال أردت التأكد من السياسات والأحكام الخاصة بالبائعين 
                    </p>
                </div>
                <a href="{{route('/blog/4')}}" class="card-read-more">اقرأ المزيد</a>
            </div>
        </div>
     </section>
     <section class="email-subscription">
        <div class="blog-subscription-box">
            <p>كن جزءًا من متابعي مدونة بازاري واشترك في نشرتنا الإخبارية ليصلك كل جديد</p>
            <div class="blog-subscription-form">
                <input type="email" id="subscription-email" placeholder="البريد الالكتروني">
                <button type="button" id="subscription-btn">
                    <i class="icon-paper-plane"></i>
                    <span>ارسال</span>
                </button>
                <p class="temp-msg" style='display:none;font-size:14px;margin-bottom:0;'>هذه الميزة ليست متوفرة حاليًا</p>
            </div>
        </div>
        <img src="{{asset('storage/img/paper-plane-path.svg')}}" alt="Paper plane with path">
     </section>
     <footer class="main-footer">
        <section class="footer-right">
            <img src="{{asset('storage/img/footer-logo.jpg')}}" alt="Bazzarry logo" class="bazzarry-logo">
        </section>
        <section class="footer-links">
            <h3 class="footer-section-title">القوانين والأحكام</h3>
            <a href="#" class="footer-pages">سياسات بازاري</a>
        </section>
        <section class="blog-footer-social">
            <h3 class="footer-section-title">وسائل التواصل</h3>
            <a href="https://www.bazzarry.com/" target="_blank" class="blog-footer-social-link">
                <span>bazzarry.com</span>
                <i class="icon-globe"></i>
            </a>
            <a href="#" target="_blank" class="blog-footer-social-link">
                <span>8000501</span>
                <i class="icon-phone"></i>
            </a>
            <a href="https://api.whatsapp.com/send?phone=967776600900&text=Source%3A%20Seller%20page" target="_blank" class="footer-social-link">
                <span>776600900</span>
                <i class="icon-whatsapp"></i>
            </a>
            <a href="https://www.facebook.com/bazzarry" target="_blank" class="blog-footer-social-link">
                <span>bazzarry@</span>
                <i class="icon-facebook"></i>
            </a>
            <a href="https://twitter.com/bazzarry" target="_blank" class="blog-footer-social-link">
                <span>bazzarry@</span>
                <i class="icon-twitter"></i>
            </a>
            <a href="https://www.youtube.com/channel/UCxyV4TB07GeCqB84l1QlrNg" target="_blank" class="blog-footer-social-link">
                <span>bazzarry@</span>
                <i class="icon-youtube"></i>
            </a>
        </section>
     </footer>
     <script>
         $(document).ready(function(){
            $("#subscription-btn").click(function(){
                $(".temp-msg").fadeIn(500).delay(2000).fadeOut(500);
            });
         });
        /*var galleryThumbs = new Swiper(".gallery-thumbs", {
            centeredSlides: true,
            centeredSlidesBounds: true,
            slidesPerView: 3,
            watchOverflow: true,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
            direction: 'vertical'
        });

        var galleryMain = new Swiper(".gallery-main", {
            loop: true,
            autoplay : true,
            watchOverflow: true,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
            preventInteractionOnTransition: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            effect: 'fade',
                fadeEffect: {
                crossFade: true
            },
            thumbs: {
                swiper: galleryThumbs
            }
        });

        galleryMain.on('slideChangeTransitionStart', function() {
        galleryThumbs.slideTo(galleryMain.activeIndex);
        });

        galleryThumbs.on('transitionStart', function(){
        galleryMain.slideTo(galleryThumbs.activeIndex);
        });*/
      </script>
</div> 