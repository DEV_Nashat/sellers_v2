<div>
    <div id="reg-page-wrapper">
        <aside id="reg-stepper">
            <header>
                <img src={{asset("/storage/img/logo-black.png")}} alt="Bazzarry logo">
                <h1>قم بانشاء حساب التاجر الخاص بك</h1>
                <p>هل لديك حساب مسبق؟</p>
                <a href="./login">قم بتسجيل الدخول إليه</a>
            </header>
            <div id="steps-progress">
                <h4>مراحل انشاء الحساب</h4>
                <div id="steps-notes">
                    <div class="step">
                        <div class="tick">1</div>
                        <p>معلومات تسجيل الدخول</p>
                    </div>
                    <div class="step">
                        <div class="tick">2</div>
                        <p>معلومات عامة</p>
                    </div>
                    <div class="step">
                        <div class="tick">3</div>
                        <p>معلومات طبيعة العمل</p>
                    </div>
                    <div class="step">
                        <div class="tick">4</div>
                        <p>معلومات المتجر والمنتجات</p>
                    </div>
                </div>
            </div>
        </aside>
        <div id="reg-steps">
            <div id="reg-slides-container">
                <div class="reg-slide">
                    <h2>معلومات تسجيل الدخول</h2>
                    <div class="controls-container">
                        <p>الحقول التي تحتوي على العلامة (*) هي حقول إلزامية</p>
                        <div class="control">
                            <label class="control-label">
                                الاسم الرباعي حسب البطاقة الشخصية
                                <span class="required-field">*</span>
                            </label>
                            <input type="text" class="control-input fslide-input" id="sfn">
                        </div>
                        <div class="control">
                            <label class="control-label">
                                رقم الجوال
                                <span class="required-field">*</span>
                            </label>
                            <input type="tel" class="control-input fslide-input" id="smn">
                        </div>
                        <span class="v-msg"></span>
                        <div class="control">
                            <label class="control-label">
                                اسم المستخدم
                                <span class="required-field">*</span>
                            </label>
                            <input type="text" class="control-input fslide-input" id="sun">
                        </div>
                        <div class="control">
                            <label class="control-label">
                                كلمة المرور
                                <span class="required-field">*</span>
                            </label>
                            <input type="password" class="control-input fslide-input" id="passwd">
                        </div>
                        <div class="full-check-control">
                            <input type="checkbox" id="sac" class="fslide-input">
                            <label for="sac">
                                اوافق على
                                <a href="#">اتفاقية تسجيل البائعين</a>
                                <span class="required-field">*</span>
                            </label>
                        </div>
                        <button class="next-slide only-btn" id="fslide-btn">الخطوة التالية</button>
                        <span class="err-msg">يُرجى ملئ كافة البيانات</span>
                    </div>
                </div>
                <div class="reg-slide">
                    <h2>معلومات عامة</h2>
                    <div class="controls-container">
                        <p>الحقول التي تحتوي على العلامة (*) هي حقول إلزامية</p>
                        <div class="selection-control">
                            <label class="control-label">
                                الجنس
                                <span class="required-field">*</span>
                            </label>
                            <div class="selection-container">
                                <div>
                                    <input type="radio" id="smg" name="sgt" checked>
                                    <label for="smg">ذكر</label>
                                </div>
                                <div>
                                    <input type="radio" id="sfg" name="sgt">
                                    <label for="sfg">أنثى</label>
                                </div>
                            </div>
                        </div>
                        <div class="control">
                            <label class="control-label">
                                رقم الهاتف
                                <span class="required-field">*</span>
                            </label>
                            <input type="tel" class="control-input sslide-input" id="spn">
                        </div>
                        <div class="control">
                            <label class="control-label">
                                الدولة
                                <span class="required-field">*</span>
                            </label>
                            <select id="sg" class="control-select">
                                <option value="0">اختر الدولة</option>
                                <option value="1">اليمن</option>
                                <option value="2">الإمارات العربية المتحدة</option>
                            </select>
                        </div>
                        <div class="control">
                            <label class="control-label">
                                المنطقة
                                <span class="required-field">*</span>
                            </label>
                            <select id="sd" class="control-select">
                                <option value="0">اختر المنطقة</option>
                                <option value="1">صنعاء</option>
                                <option value="2">عدن</option>
                                <option value="5">دبي</option>
                            </select>
                        </div>
                        <div class="control">
                            <label class="control-label">
                                العنوان
                                <span class="required-field">*</span>
                            </label>
                            <input type="text" class="control-input sslide-input" id="sa">
                        </div>
                        <div class="control">
                            <label class="control-label">
                                أقرب معلم
                                <span class="required-field">*</span>
                            </label>
                            <input type="text" class="control-input sslide-input" id="snb">
                        </div>
                        <div class="slide-btns">
                            <button class="previous-slide">الخطوة السابقة</button>
                            <button class="next-slide" id="sslide-btn">الخطوة التالية</button>
                        </div>
                        <span class="err-msg" style="display: none;">يُرجى ملئ كافة البيانات</span>
                    </div>
                </div>
                <div class="reg-slide">
                    <h2>معلومات طبيعة العمل</h2>
                    <div class="controls-container">
                        <p>الحقول التي تحتوي على العلامة (*) هي حقول إلزامية</p>
                        <div class="selection-control">
                            <label class="control-label">
                                طبيعة العمل
                                <span class="required-field">*</span>
                            </label>
                            <div class="selection-container">
                                <div>
                                    <input type="radio" id="spw" name="sbt" value="spw" checked>
                                    <label for="spw">شخصي</label>
                                </div>
                                <div>
                                    <input type="radio" id="sbw" name="sbt" value="sbw">
                                    <label for="sbw">عمل</label>
                                </div>
                            </div>
                        </div>
                        <div id="spw-container">
                            <div class="selection-control" id="sidt">
                                <label class="control-label">
                                    نوع اثبات الهوية
                                    <span class="required-field">*</span>
                                </label>
                                <div class="selection-container">
                                    <div>
                                        <input type="radio" id="sidc" name="sidp" checked>
                                        <label for="sbw">بطاقة شخصية</label>
                                    </div>
                                    <div>
                                        <input type="radio" id="spp" name="sidp">
                                        <label for="spw">جواز سفر</label>
                                    </div>
                                </div>
                            </div>
                            <div class="control" id="sdno">
                                <label class="control-label">
                                    رقم المستند
                                    <span class="required-field">*</span>
                                </label>
                                <input type="tel" class="control-input tslide-input" id="sidno">
                            </div>
                        </div>
                        <div class="slide-btns">
                            <button class="previous-slide">الخطوة السابقة</button>
                            <button class="next-slide" id="tslide-btn">الخطوة التالية</button>
                        </div>
                        <span class="err-msg">يُرجى ملئ كافة البيانات</span>
                    </div>
                </div>
                <div class="reg-slide">
                    <h2>معلومات المتجر والمنتجات</h2>
                    <div class="controls-container">
                        <p>الحقول التي تحتوي على العلامة (*) هي حقول إلزامية</p>
                        <div class="control">
                            <label class="control-label">
                                اسم المتجر
                                <span class="required-field">*</span>
                            </label>
                            <input type="text" class="control-input ftslide-input" id="ssn">
                        </div>
                        <!--  <button type="button" class="upload-btn" id="uspl-btn">
                            + إضافة موقع المتجر
                            <span>*</span>
                        </button>
                       <div class="udinfo" id="scl"></div>-->
                        <div class="slide-btns">
                            <button class="previous-slide">الخطوة السابقة</button>
                            <button id="sreg-btn">انشاء حساب</button>
                        </div>
                        <span class="err-msg">يُرجى ملئ كافة البيانات</span>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <script>
    $(document).ready(function() {
        $("#smn").keyup(function() {
            if ($(this).val().trim().length > 2 && $(this).val().trim().length < 10) {
                var start = $(this).val().substr(0, 2);
                if (start != "73" && start != "77") {
                    $(this).parent().css({
                        "border": "1px solid red"
                    });
                    $(this).parent().next(".v-msg").text("رقم الجوال يجب ان يبدأ بـ 73 أو بـ 77");
                }
            } else if ($(this).val().trim().length > 9) {
                $(this).parent().css({
                    "border": "1px solid red"
                });
                $(this).parent().next(".v-msg").text("رقم الجوال يجب ألا يتجاوز 9 أرقام");
            } else {
                $(this).parent().next(".v-msg").text("");
                $(this).parent().css({
                    "border": "1px solid #d1d1d1"
                });
            }
        });
    });
    </script>
</div>
