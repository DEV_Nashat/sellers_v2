<div>
        <header class="fixed">
            <nav class="main-nav">
                <a href="./" class="nav-logo">
                    <img src="{{ asset('/storage/img/logo.svg') }}" alt="Bazzarry Logo">
                </a>
                <ul class="nav-menu main-nav-only-child">
                    <li class="nav-menu-item">
                        <a href="{{route('kb')}}" class="menu-item-content active-tab">
                            قاعدة المعرفة
                        </a>
                    </li>
                </ul>
                <button type="button" id="menu-collapse">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            </nav>
        </header>
        <section class="c-questions" id="common-questions">
            <h3 class="has-decoration">كيف يمكننا مساعدتك</h3>
            <div class="tabs">
                <div class="tab">
                    <header>
                        <i class="icon-plus"></i>
                        <h4>كيف أستطيع إدارة حساب البائع في بازاري؟</h4>
                    </header>
                    <div class="answer">
                        <p>كل بائع مسجل ومعتمد لدى بازاري يستطيع بيع منتجاته وإدارة متجره في بازاري عن طريق واجهتنا الداخلية والتي تسمى معمل البائع. 
                            <a href="#">انقر هنا</a> 
                            للوصول الى معمل البائع الخاص بك.</p>
                    </div>
                </div>
                <div class="tab">
                    <header>
                        <i class="icon-plus"></i>
                        <h4>ماذا يعني البيع عن طريق موقع بازاري؟</h4>
                    </header>
                    <div class="answer">
                        <p>بازاري هو منصة رقمية محلية ترحب بكل الباعة الذين يملكون نشاطا تجاريا مهما كان حجمه صغيرا او كبيرا، حيث ان هدفنا في بازاري دعم الأعمال التجارية في اليمن حاليا وخلق فرصة للتجار لتسويق منتجاتهم وتوسيع قاعدة عملائهم وزيادة مبيعاتهم عبر الانترنت.</p>
                    </div>
                </div>
                <div class="tab">
                    <header>
                        <i class="icon-plus"></i>
                        <h4>مالذي أستطيع بيعه على موقع بازاري؟</h4>
                    </header>
                    <div class="answer">
                        <p>يوجد على موقع بازاري العديد من فئات الأعمال التي تتضمن منتجات تناسب احتياجات العملاء وتلبي رغباتهم مثل : الأزياء وموضة، الالكترونيات، الجوالات والحواسيب وملحقاتها، الصحة والجمال، أدوات المنزل والمطبخ، مستلزمات حيوانات أليفة - عطور، تحف وهدايا، العاب، أدوات رياضة ولياقة بدنية، أثاث.</p>
                    </div>
                </div>
                <div class="tab">
                    <header>
                        <i class="icon-plus"></i>
                        <h4>كيف انشئ حساب بائع على موقع بازاري؟</h4>
                    </header>
                    <div class="answer">
                        <p>انشاء حساب بائع على بازاري أمر في غاية السهولة، فقط عليك تعبئة الحقول المطلوبة في صفحة التسجيل وتقديم المستندات التالية لإكمال التسجيل:
                        <p class="listed">صورة من بطاقتك الشخصية</p> 
                        <p class="listed">صورة من سجلك التجاري إن وجد</p> 
                        وبمجرد الانتهاء ستحصل على الموافقة وسيعمل متجرك في غضون أيام.
                        </p>
                    </div>
                </div>
                <div class="tab">
                    <header>
                        <i class="icon-plus"></i>
                        <h4>كم تكلفة بيع منتجاتي على بازاري؟</h4>
                    </header>
                    <div class="answer">
                        <p>لكل فئة رسوم وعمولات محددة ستظهر لك عند اختيارك الفئات الخاصة بك اثناء التسجيل.</p>
                    </div>
                </div>
                <div class="tab">
                    <header>
                        <i class="icon-plus"></i>
                        <h4>كيف أضيف مخزوني في بازاري؟</h4>
                    </header>
                    <div class="answer">
                        <p>
                            لبدء البيع ، ستحتاج إلى إضافة مخزون إلى متجر البائع الخاص بك. يعد عرض المنتجات في بازاري أمرًا بسيطًا حيث أنك ستحتاج  إلى الأساسيات كصور رائعة ووصفًا للمنتج و المواصفات التي يتمتع بها المنتج الخاص بك، لتفاصيل اكثر حول إضافة المخزون وإدارة الحساب
                            <a href="#">اضغط هنا</a>
                        </p>
                    </div>
                </div>
                <div class="tab">
                    <header>
                        <i class="icon-plus"></i>
                        <h4>ماهي السياسة المتبعة عند إرجاع المنتجات او إلغاء الطلب؟</h4>
                    </header>
                    <div class="answer">
                        <p>
                        يحدث أحيانا أن يرجع العميل الطلب لأسباب معينة أو يطلب استبداله، هناك سياسات محددة للاسترجاع والاستبدال لدى بازاري يمكنك الاطلاع عليها   
                            <a href="#">من هنا</a>
                        </p>
                    </div>
                </div>
                <div class="tab">
                    <header>
                        <i class="icon-plus"></i>
                        <h4>كيف أقوم بتصوير ووصف المنتج الخاص بي؟</h4>
                    </header>
                    <div class="answer">
                        <p>
                            يوفر لك بازاري ارشادات هامة حول طريقة تصوير منتجك وكتابة وصف مميز وجاذب يمكنك زيارة الروابط التالية لمعرفة تفاصيل أكثر حول: 
                            <p class="listed">آلية تصوير المنتج، <a href="#">اضغط هنا</a></p>
                            <p class="listed">كتابة وصف المنتج، <a href="#">اضغط هنا</a></p>
                        </p>
                    </div>
                </div>
                <div class="tab">
                    <header>
                        <i class="icon-plus"></i>
                        <h4>هل بإمكاني اضافة صور جاهزة لمنتجي ؟</h4>
                    </header>
                    <div class="answer">
                        <p>
                        نعم، في حال كان المنتج يملك شهرة واسعة وتتوفر له صور على الانترنت بدقة واضحة وبقياسات مطابقة لسياسات رفع الصور في بازاري.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <footer class="seller-footer">
            <div class="subscription-box">
                <p>انضم لمتابعي بازاري لمعرفة كل جديد عبر الاشتراك بالنشرة الإخبارية</p>
                <div class="subscription-input">
                    <label>البريد الالكتروني أو رقم الواتساب *</label>
                    <input type="text" id="subscriber-contact">
                    <button type="button" id="subscription-btn" class="pr-btn">
                        <i class="icon-paper-plane"></i>
                        <span>اشتراك</span>
                    </button>
                </div>
            </div>
            <div class="column">
                <h3>القوانين والأحكام</h3>
                <a href="#">سياسات بازاري</a>
            </div>
            <div class="column">
                <h3>وسائل التواصل</h3>
                <a href="https://www.bazzarry.com/" target="_blank" class="footer-social-link">
                    <span>bazzarry.com</span>
                    <i class="icon-globe"></i>
                </a>
                <a href="#" class="footer-social-link">
                    <span>8000501</span>
                    <i class="icon-phone"></i>
                </a>
                <a href="#" class="footer-social-link">
                    <span>sellers@bazzarry.com</span>
                    <i class="icon-mail-1"></i>
                </a>
                <a href="https://api.whatsapp.com/send?phone=967776600900&text=Source%3A%20Seller%20page" target="_blank" class="footer-social-link">
                    <span>776600900</span>
                    <i class="icon-whatsapp"></i>
                </a>
                <a href="https://www.facebook.com/bazzarry" target="_blank" class="footer-social-link">
                    <span>bazzarry@</span>
                    <i class="icon-facebook"></i>
                </a>
                <a href="https://twitter.com/bazzarry" target="_blank" class="footer-social-link">
                    <span>bazzarry@</span>
                    <i class="icon-twitter"></i>
                </a>
                <a href="https://www.youtube.com/channel/UCxyV4TB07GeCqB84l1QlrNg" target="_blank" class="footer-social-link">
                    <span>bazzarry@</span>
                    <i class="icon-youtube"></i>
                </a>
            </div>
        </footer>
        <script>
            $(document).ready(function(){
                window.location.href = "#common-questions";
            });
        </script>
    </div>
