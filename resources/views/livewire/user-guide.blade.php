<div style="min-height:100vh;display:flex;flex-flow:column;justify-content: space-between;">
    <header class="fixed">
        <nav class="main-nav">
            <a href="./" class="nav-logo">
                <img src="{{ asset('storage/img/logo.svg') }}" alt="Bazzarry Logo">
            </a>
            <ul class="nav-menu main-nav-only-child">
                <li class="nav-menu-item">
                    <a href="#" class="menu-item-content active-tab">
                        قاعدة المعرفة
                    </a>
                </li>
            </ul>
            <button type="button" id="menu-collapse">
                <span></span>
                <span></span>
                <span></span>
            </button>
        </nav>
    </header>
    <section class="knowledge" id="knowledge-base">
        <h3 class="has-decoration">قاعدة المعرفة</h3>
        <div id="kb-topics-container">
            <div class="kb-main-topic">
                <a href="{{ route('blog') }}">
                    <img src="{{ asset('storage/img/blog.png') }}" alt="Blog icon" class="kb-topic-icon">
                    <div class="inner">
                        <h3 class="kb-topic-title">المدونة والمقالات</h3>
                        <hr>
                        <p class="kb-topic-counts">4 مواضيع</p>
                    </div>
                </a>
            </div>
            <div class="kb-main-topic">
                <a href="{{ route('faqs') }}">
                    <img src="{{ asset('storage/img/faqs.png') }}" alt="Faqs icon" class="kb-topic-icon">
                    <div class="inner">
                        <h3 class="kb-topic-title">الأسئلة الشائعة</h3>
                        <hr>
                        <p class="kb-topic-counts">9 مواضيع</p>
                    </div>
                </a>
            </div>
        </div>
    </section>
    <footer class="seller-footer">
        <div class="subscription-box">
            <!--<p>انضم لمتابعي بازاري لمعرفة كل جديد عبر الاشتراك بالنشرة الإخبارية</p>
            <div class="subscription-input">
                <label>البريد الالكتروني أو رقم الواتساب *</label>
                <input type="text" id="subscriber-contact">
                <button type="button" id="subscription-btn" class="pr-btn">
                    <i class="icon-paper-plane"></i>
                    <span>اشتراك</span>
                </button>
            </div>-->
            <img src="{{ asset('storage/img/logo.svg') }}" alt="Bazzarry Logo" class="footer-img">
            <p class="footer-txt">بازاري هو أول وأكبر موقع تسوق عبر الإنترنت في اليمن</p>
            <p class="footer-txt">© بازاري <?php echo date('Y'); ?></p>
        </div>
        <div class="column">
            <h3>القوانين والأحكام</h3>
            <a href="#">سياسات بازاري</a>
        </div>
        <div class="column">
            <h3>وسائل التواصل</h3>
            <a href="https://www.bazzarry.com/" target="_blank" class="footer-social-link">
                <span>bazzarry.com</span>
                <i class="icon-globe"></i>
            </a>
            <a href="#" class="footer-social-link">
                <span>8000501</span>
                <i class="icon-phone"></i>
            </a>
            <a href="#" class="footer-social-link">
                <span>sellers@bazzarry.com</span>
                <i class="icon-mail-1"></i>
            </a>
            <a href="https://api.whatsapp.com/send?phone=967776600900&text=Source%3A%20Seller%20page" target="_blank"
                class="footer-social-link">
                <span>776600900</span>
                <i class="icon-whatsapp"></i>
            </a>
            <a href="https://www.facebook.com/bazzarry" target="_blank" class="footer-social-link">
                <span>bazzarry@</span>
                <i class="icon-facebook"></i>
            </a>
            <a href="https://twitter.com/bazzarry" target="_blank" class="footer-social-link">
                <span>bazzarry@</span>
                <i class="icon-twitter"></i>
            </a>
            <a href="https://www.youtube.com/channel/UCxyV4TB07GeCqB84l1QlrNg" target="_blank"
                class="footer-social-link">
                <span>bazzarry@</span>
                <i class="icon-youtube"></i>
            </a>
        </div>
    </footer>
</div>
