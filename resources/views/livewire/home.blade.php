<div>
    <header class="fixed">
        <div class="main-nav">
            <a href="#" class="nav-logo">
                <img src="{{asset('storage/img/logo.svg')}}" alt="Bazzarry Logo">
            </a>
            <ul class="nav-menu">
                <li class="nav-menu-item">
                    <a href="#join-us" class="menu-item-content active-tab">
                        انضم لنا
                    </a>
                </li>
                <li class="nav-menu-item">
                    <a href="#features" class="menu-item-content">
                        لماذا بازاري
                    </a>
                </li>
                <li class="nav-menu-item">
                    <a href="#delegates" class="menu-item-content">
                        تواصل معنا
                    </a>
                </li>
                <li class="nav-menu-item">
                    <a href="{{route('kb')}}" class="menu-item-content">
                        قاعدة المعرفة
                    </a>
                </li>
            </ul>
            <button type="button" id="menu-collapse">
                <span></span>
                <span></span>
                <span></span>
            </button>
        </div>
    </header>
    <div class="full intro" id="join-us">
        <div class="skewed">
            <h1>التجارة الألكترونية</h1>
            <h3>بطريقة لم تتخليها من قبل</h3>
            <a href="{{ route('reg') }}" class="pr-btn">انضم لنا</a>
            <img src="{{Asset('storage/img/lines.png')}}" alt="dashes" class="intro-drcoration">
        </div>
    </div>
    <div class="full features" id="features">
        <div class="section-title">
            <h3 class="has-decoration">لماذا بازاري</h3>
        </div>
        <div class="features-container">
            <div class="feature">
                <img src="{{asset('storage/img/clients.png')}}" alt="clients icon">
                <h4>توسيع قاعدة عملائك</h4>
                <p>اجعل انتشارك أسرع وعملاءك أكثر مع بازاري لأنه يستهدف في إعلاناته وتسويقه معظم المحافظات اليمنية</p>
            </div>
            <div class="feature">
                <img src="{{asset('storage/img/shipping.png')}}" alt="clients icon">
                <h4>الشحن والتوصيل</h4>
                <p>مع بازاري لن تقلق بشأن التوصيل لأننا سنحرص على تقديم أفضل خدمة توصيل ممكنة</p>
            </div>
            <div class="feature">
                <img src="{{asset('storage/img/profits.png')}}" alt="clients icon">
                <h4>زيادة مبيعاتك</h4>
                <p>إن ظهور منتجاتك على بازاري يمنحك فرصة ذهبية لزيادة مبيعاتك لأنها ستصل لقاعدة واسعة من العملاء المتفاعلين</p>
            </div>
        </div>
    </div>
    <div class="delegates" id="delegates">
        <div class="section-title">
            <h3 class="has-decoration">مندوبي بازاري</h3>
            <p class="section-desc">الأن يمكنك التواصل معنا عبر مندوبي بازاري عبر النقر على أحدهم</p>
        </div>
        <div class="delegates-areas">
            <div class="area">
                <div class="single-area">
                    <div class="area-map-gps">
                        <img src="{{asset('storage/img/adenMap.png')}}" alt="Aden Map">
                        <h3><i class="icon-location"></i>عدن</h3>
                    </div>
                    <div class="delegates-container">
                        <div class="single-delegate">
                            <a href="https://wa.me/+967771417162" target="_blank">
                                <i class="icon-whatsapp"></i>
                                <h5>أحمد وليد</h5>
                            </a>
                        </div>
                        <div class="single-delegate">
                            <a href="https://wa.me/+967771417481" target="_blank">
                            <i class="icon-whatsapp"></i>
                                <h5>غسان العفيفي</h5>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="area">
                <div class="single-area">
                    <div class="area-map-gps">
                        <img src="{{asset('storage/img/sanaaMap.png')}}" alt="Sana'a Map">
                        <h3><i class="icon-location"></i>صنعاء</h3>
                    </div>
                    <div class="delegates-container">
                        <div class="single-delegate">
                            <a href="https://wa.me/+967779000518">
                            <i class="icon-whatsapp"></i>
                                <h5>وليد لاهب</h5>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="contact-us" id="contacts">
        <div class="contact-sections">
            
            <div class="right">
                <img src="{{asset('storage/img/logo.svg')}}" alt="Bazzarry Logo" class="footer-img">
                <p class="footer-txt">بازاري هو أول وأكبر موقع تسوق عبر الإنترنت في اليمن</p>
                <p class="footer-txt">© بازاري <?php echo date("Y") ?></p>
            </div>
            <div class="left">
                <div class="contact-img">
                    <img src="{{asset('storage/img/contact-ways.png')}}" alt="Contact Ways Picture">
                </div>
                <div class="contact-info">
                    <a href="https://www.bazzarry.com/" target="_blank">bazzarry.com<i class="icon-globe"></i></a>
                    <p>8000501 <i class="icon-phone"></i></p>
                    <p>sellers@bazzarry.com <i class="icon-mail-1"></i></p>
                    <a href="https://api.whatsapp.com/send?phone=967776600900&text=Source%3A%20Seller%20page" target="_blank">776600900<i class="icon-whatsapp"></i></a>
                    <a href="https://www.facebook.com/bazzarry" target="_blank">bazzarry@<i class="icon-facebook"></i></a>
                    <a href="https://twitter.com/bazzarry" target="_blank">bazzarry@<i class="icon-twitter"></i></a>
                    <a href="https://www.youtube.com/channel/UCxyV4TB07GeCqB84l1QlrNg" target="_blank">bazzarry@<i class="icon-youtube"></i></a>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            window.location.href = "#join-us";

        //     $('.pr-btn').on('click',function(){
        //         regPath = "{{URL::to('reg')}}" ;
        //         window.location.href = regPath;
        //     });
         });
    </script>

</div>