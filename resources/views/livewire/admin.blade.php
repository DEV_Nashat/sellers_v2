<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{asset('storage/img/favicon.ico')}}" type="image/x-icon">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="{{asset('css/dashboard.css')}}">
    <title>Admin Login</title>
    <style>
        /* Control Style Starts Here */
        img{
            width: 250px;
            height: auto;
        }
        .control{
            position: relative;
            width: 94%;
            max-width: 400px;
            height: 46px;
            border: 1px solid #2feebb;
            margin-bottom: 20px;
            border-radius: 35px;
        }
        .animated{
            position: absolute;
            transition: .6s ease;
            z-index: 1;
            color: #2feebb;
            top: 7px;
            right: 15px;
            font-size: 14px; 
            background: transparent;
            padding: 0;
            pointer-events: none;
        }
        .control-input{
            width: calc(100% - 20px);
            max-width: 400px;
            border-radius: 20px;
            padding: 5px 10px;
            font-size: 18px;
            position: absolute;
            bottom: 4px;
            right: 10px;
        }
        .control .ipass{
            position: absolute;
            top: 7px;
            left: 10px;
            color: #928f8f;
            font-size: 18px;
            z-index: 2;
            cursor: pointer;
        }
        .primary-btn{
            width: 94%;
            max-width: 400px;
            height: 46px;
            background: #2feebb !important;
            color: #fff !important;
            font-size: 18px !important;
            border-radius: 35px !important;
            text-shadow: 2px 2px 1px #777474;
            margin-bottom: 10px;
            border: none;
            padding: 0 !important;
        }
        .controls-container{
            align-items: center;
        }
        /* Control Style Ends Here */
    </style>
</head> 
<body>
    <div id="login-page-wrapper">
        <header>
            <img src="{{asset('/storage/img/logo-black.png')}}" alt="Bazzarry logo"> 
            <h1>سجل دخولك لإدارة التجار</h1>
        </header>
        <div id="login-box">
            <div class="controls-container">
                <div class="control">
                    <label class="animated">اسم المستخدم</label>
                    <input type="text" id="sun" class="control-input">
                </div>
                <div class="control">
                    <label class="animated">كلمة المرور</label>
                    <input type="password" id="spw" class="control-input">
                    <i class="ipass icon-eye"></i>
                </div>
                <button type="button" id="slog-btn" class="primary-btn">متابعة</button>
                <p class="err-msg"></p>
            </div>
        </div>
    </div>

    <script src= "{{asset('js/jquery-3.3.1.min.js')}}"></script> 
    <script src="{{asset('js/dashboard.js')}}"></script>
</body>
</html>