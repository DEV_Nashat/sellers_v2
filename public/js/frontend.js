$(document).ready(function(){

    $("#fslide-btn").click(function(){
        var emptyInput = false;
        $('.fslide-input').each(function() {
            if ($(this).val() == '')
                emptyInput = true;
        });
        if(!emptyInput && $('#sac').is(':checked'))
        {
            var tabIndex = $(this).index(".next-slide");
            var slide = (tabIndex + 1) * 100;
            $(".reg-slide").eq(tabIndex).css({
                "transform":"translateX(100%)"
            });
            $(".reg-slide").siblings().css({
                "transform":"translateX("+ slide + "%)"
            });
            $(".tick").eq(tabIndex).html("<i class='icon-ok'></i>");
        }
        else
        {
            $(".err-msg").fadeIn(400).delay(10000).fadeOut(400);
        }
    });

    $("#sslide-btn").click(function(){
        var emptyInput = false;
        $('.sslide-input').each(function() {
            if ($(this).val() == '')
                emptyInput = true;
        });
        if(!emptyInput && $("#sg option:selected").val() != 0 && $("#sd option:selected").val() != 0)
        {
            var tabIndex = $(this).index(".next-slide");
            var slide = (tabIndex + 1) * 100;
            $(".reg-slide").eq(tabIndex).css({
                "transform":"translateX(100%)"
            });
            $(".reg-slide").siblings().css({
                "transform":"translateX("+ slide + "%)"
            });
            $(".tick").eq(tabIndex).html("<i class='icon-ok'></i>");
        }
        else
        {
            $(".err-msg").fadeIn(400).delay(10000).fadeOut(400);
        }
    });

    $("#tslide-btn").click(function(){
        var emptyInput = false;
        $('.tslide-input').each(function() {
            if ($(this).val() == '')
                emptyInput = true;
        });
        if(!emptyInput)
        {
            var tabIndex = $(this).index(".next-slide");
            var slide = (tabIndex + 1) * 100;
            $(".reg-slide").eq(tabIndex).css({
                "transform":"translateX(100%)"
            });
            $(".reg-slide").siblings().css({
                "transform":"translateX("+ slide + "%)"
            });
            $(".tick").eq(tabIndex).html("<i class='icon-ok'></i>");
        }
        else
        {
            $(".err-msg").fadeIn(400).delay(10000).fadeOut(400);
        }
    });

    $("#sreg-btn").click(function(){
        var emptyInput = false;
        $('.ftslide-input').each(function() {
            if ($(this).val() == '')
                emptyInput = true;
        });
        if(!emptyInput)
        {
            var sfn = $("#sfn").val().trim();
            var smn = $("#smn").val().trim();
            var sun = $("#sun").val().trim();
            var spw = $("#spw").val().trim();
            var sgt = $('input[name="sgt"]:checked').val();
            var spn = $("#spn").val().trim();
            var sg = $("#sg option:selected").val();
            var sd = $("#sd option:selected").val();
            var sa = $("#sa").val().trim();
            var snb = $("#snb").val().trim();
            var sbt = $('input[name="sbt"]:checked').val();
            var sidp = $('input[name="sidp"]:checked').val();
            var sidno = $("#sidno").val().trim();
            var ssn = $("#ssn").val();
            //var scl= $("#scl").text();

            $.ajax({
                url: "./new",
                type: "POST",
                data: {
                    sfn:sfn,
                    smn:smn,
                    sun:sun,
                    spw:$('#spw').is(':checked')?1:($('#sbw').is(':checked')?2:1),
                    sfg:$('#smg').is(':checked')?1:($('#sfg').is(':checked')?0:1),
                    sidc:$('#sidc').is(':checked')?1:($('#spp').is(':checked')?2:1),
                    sgt:sgt,
                    spn:spn,
                    sd:sd,
                    sa:sa,
                    snb:snb,
                    sbt:sbt,
                    sidp:sidp,
                    sidno:sidno,
                    ssn:ssn,
                    //scl:scl,
                    passwd:$('#passwd').val()
                },
                beforeSend:function(){
                    $("#sreg-btn").prop("disabled",true);
                },
                success: function(data){
                    $("#sreg-btn").prop("disabled",false);
                   
                    if(data==="1")
                      window.location.href='./login'
                    else
                    alert('This user could be existed before');
                   
                }
            });
        }
        else
        {
            $(".err-msg").fadeIn(400).delay(10000).fadeOut(400);
        }
    });

    $(".previous-slide").click(function(){
        var tabIndex = $(this).index(".previous-slide");
        var slide = (tabIndex) * 100;
        $(".reg-slide").eq(tabIndex).css({
            "transform":"translateX(-100%)"
        });
        $(".reg-slide").siblings().css({
            "transform":"translateX("+ slide + "%)"
        });
    });

    $('input[type=radio][name=sbt]').change(function() {
        if (this.value == 'spw') {
            $("#sidt").fadeIn(100);
            $("#sdno label").text("رقم الهوية *");
        }
        else if (this.value == 'sbw') {
            $("#sidt").fadeOut(500);
            $("#sdno label").text("رقم الترخيص أو مزاولة المهنة *");
        }
    }); 

    $(".control-input").focus(function(){
        if($(this).val().trim() == "")
        {
            $(this).siblings(".animated").css({
                "font-size" : "12px",
                "top" : "-16px",
                "background" : "#fff",
                "padding" : "2px 4px",
                "font-weight" : "bold"
            });
        }
    });

    $(".control-input").blur(function(){
        if($(this).val().trim() == "")
        {
            $(this).siblings(".animated").css({
                "font-size" : "14px",
                "top" : "7px",
                "background" : "transparent",
                "padding" : "0px",
                "font-weight" : "normal"
            });
        }
    });

    var x = document.getElementById("#scl");
    function getLocation() 
    {
        if (navigator.geolocation) 
        {
            navigator.geolocation.getCurrentPosition(showPosition);
        } 
        else {
            x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }

    function showPosition(position) 
    {
        $("#scl").text("احداثيات موقعك الحالي: " + position.coords.latitude +
        " , " + position.coords.longitude);
    }

    $("#uspl-btn").click(function(){
        $("#scl").fadeIn(400).css({
            "display" : "flex",
            "border-bottom" : "1px solid #e9e3e3"
        });
    });

    $("#sdocfile").change(function(){
        $(this).next(".udinfo").fadeIn(400).css({
            "display" : "flex",
            "border-bottom" : "1px solid #e9e3e3"
        });
        $("#sdocpic").text($("#sdocfile").val().replace(/.*(\/|\\)/, ''));
    });

    $("#slogopic").change(function(){
        $(this).next(".udinfo").fadeIn(400).css({
            "display" : "flex",
            "border-bottom" : "1px solid #e9e3e3"
        });
        $("#splogopicn").text($("#slogopic").val().replace(/.*(\/|\\)/, ''));
    });

    $("#spimgs").change(function(){
        var uploadInput = $("#spimgs");
        $(this).next(".udinfo").fadeIn(400).css({
            "display" : "flex",
            "border-bottom" : "1px solid #e9e3e3"
        });
        $("#sppics").text("عدد الصور التي تم اختيارها: " + uploadInput[0].files.length + "صور");
    });

    $("#slog-btn").click(function(){
        if($("#sun").val().trim() != "" && $("#spw").val().trim() != "")
        {
            var username = $("#sun").val().trim();
            var userpwd = $("#spw").val().trim();
            $.ajax({
                url: "./admin/auth",
                type: "POST",
                data: {
                    username:username,
                    userpwd:userpwd
                },
                success: function(data){
                    if(data==="1")
                      window.location.href='./pendding'
                    else
                    $(".err-msg").text("بيانات الدخول إلى الحساب غير صحيحة").fadeIn(400).delay(4000).fadeOut(400);
                }
            });
        }
        else
        {
            $(".err-msg").text("يُرجى التأكد من ملئ كافة البيانات").fadeIn(400).delay(4000).fadeOut(400);
        }
    });

    $(".upload-btn").click(function(e){
        e.preventDefault();
    })

    $(".ipass").click(function(){
        $(this).toggleClass("icon-eye-off").siblings(".control-input").attr("type",$(this).hasClass("icon-eye-off")?"text":"password");
    });
});