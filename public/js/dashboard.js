$(document).ready(function(){

    $(".control-input").focus(function(){
        if($(this).val().trim() == "")
        {
            $(this).siblings(".animated").css({
                "font-size" : "12px",
                "top" : "-16px",
                "background" : "#fff",
                "padding" : "2px 4px",
                "font-weight" : "bold"
            });
        }
    });

    $(".control-input").blur(function(){
        if($(this).val().trim() == "")
        {
            $(this).siblings(".animated").css({
                "font-size" : "14px",
                "top" : "7px",
                "background" : "transparent",
                "padding" : "0px",
                "font-weight" : "normal"
            });
        }
    });
    
    $("#slog-btn").click(function(){
        if($("#sun").val().trim() != "" && $("#spw").val().trim() != "")
        {
            var username = $("#sun").val().trim();
            var userpwd = $("#spw").val().trim();
            $.ajax({
                url: "./admin/check",
                type: "POST",
                data: {
                    username:username,
                    userpwd:userpwd
                },
                success: function(data){
                    if(data==="1")
                      window.location.href='./admin/dashboard'
                    else 
                    $(".err-msg").text("بيانات الدخول إلى الحساب غير صحيحة").fadeIn(400).delay(4000).fadeOut(400);
                }
            });
            
        }
        else
        {
            $(".err-msg").text("يُرجى التأكد من ملئ كافة البيانات ").fadeIn(400).delay(4000).fadeOut(400);
        }
    });

    $(document).on("click",".mini-approve",function(){
        var sid = $(this).parent().attr("sid");
       $.ajax({
            url: "../admin/state",
            type: "POST",
            data: {
                sid:sid,
                state:3
            },
            success: function(data){
                if(data == "Updated successfully")
                {
                    $(".popup-success-msg").fadeIn(400).css({"display":"flex"});
                }
                else
                {
                    $(".popup-error-msg").fadeIn(400).css({"display":"flex"});
                }
            }
        });
    }); 

    $(".scsmdl-close-btn").click(function(){
        $(".popup-success-msg").fadeOut(400);
    });

    $(".errmdl-close-btn").click(function(){
        $(".popup-success-msg").fadeOut(400);
    });

    
    $(".model-discard").click(function(){
        $(".popup").fadeOut(400);
        $("#reject-txt").val("");
    });

    $(document).on("click",".mini-discard",function(){
        $("#sreject-box").attr("sid" , $(this).parent(".options").attr("sid"));
        $("#reject-txt").val("");
        $(".popup").fadeIn(400).css({
            "display" : "flex"
        });
    }); 

    $("#reject-seller-btn").click(function(){
        if($("#reject-txt").val().trim() != "")
        {
            var sid = $("#sreject-box").attr("sid");
            $.ajax({
                url: "../admin/reject",
                type: "POST",
                data: {sid:sid,reason:$("#reject-txt").val().trim(),state:4},
                success: function(data){
                    if(data == "Updated successfully")
                    {
                        $("#sreject-box").hide();
                        $("#sreject-model").append('<div class="model-result-box"><div class="icon-decorative"><i class="icon-ok"></i></div><p>تمت عملية رفض التاجر بنجاح</p><button type="button" class="model-result-close-btn">إغلاق</button></div>');
                    }
                    else
                    {
                        $("#sreject-box").hide();
                        $("#sreject-model").append('<div class="model-result-box"><div class="icon-decorative"><i class="icon-emo-unhappy"></i></div><p>حدث خطأ في عملية رفض التاجر<br>يُرجى المحاولة لاحقًا</p><button type="button" class="model-result-close-btn">إغلاق</button></div>');
                    }
                    $("#reject-txt").val("");
                }
            });
        }
        else
        {
            $(".model-msg").text("يُرجى إضافة سبب الرفض").fadeIn(400).delay(3000).fadeOut(400);
        }
    });

    $(document).on("click" , ".model-result-close-btn" , function(){
        $(".popup").fadeOut(400,function(){
            $(".model-result-box").remove();
            $("#sreject-box").show();
        });
    });


    $("#edit-content-btn").click(function(){
        $(".popup").fadeIn(400).css({"display" : "flex"}).delay(1500).fadeOut(400);
        $('.editable').attr("contenteditable" , "true");
        $('.editable').css({"background" : "#ccc"});
    });

    $("#upload-logo").click(function(){
        $(".model-img").attr("src",$(this).siblings("img").attr("src"));
        $(".popup-img").fadeIn().css({"display":"flex"});
    });

    $("#signout-btn").click(function(){
        window.location.href='../logout';
    });

    $(document).on("click",".edit-tag",function(){
        $("#notifier-sound").trigger("play");
        var sectionTitle = $(this).siblings("h3").text();
        $(this).parent().append("<span class='discard-changes'>تراجع</span>");
        $(".popup .msg").children("p").text("يمكنك الأن تعديل "+sectionTitle);
        $(".popup").fadeIn(400).css({"display":"flex"}).delay(2000).fadeOut(400);
        $(this).parent().parent().css({"background":"#f4fffc"});
        $(this).parent().siblings(".key-value").children(".editable").attr("contenteditable","true").css({"border-color":"#606060"});
        $(this).removeClass("edit-tag").addClass("save-edits-tag").text("حفظ التعديلات");
        $(this).attr("id","edit-"+$(this).attr("for"));
    });

    $(document).on("click",".discard-changes",function(){
        var ele = $(this).parent().siblings(".key-value").children(".editable");
        $(this).parent().parent().css({"background":"#fff"});
        $(this).parent().siblings(".key-value").children(".editable").attr("contenteditable","false").css({"border-color":"transparent"});
        $(this).siblings("span").removeClass("save-edits-tag").addClass("edit-tag").text("تعديل البيانات");
        $(this).siblings("span").removeAttr("id");
        $(this).remove();
        ele.each(function( index ) {
            $(this).text($(this).attr("data"));
        });
    });

    $(".model-close").click(function(){
        $(".popup-img").fadeOut(500,function(){
            $("#logo-input").val("");
            $(".upload-btn").remove();
        });
    });

    $(".trigger-upload").click(function(){
        var uploadInput = $(this).attr("tag");
        $("#"+uploadInput).click();
    });

    $("#logo-input").change(function () {
        if (this.files && this.files[0]) 
        {
            var file = this.files[0];
            var fileType = file["type"];
            var validImageTypes = ["image/jpg", "image/jpeg", "image/png"];
            if ($.inArray(fileType, validImageTypes) > 0) 
            {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.model-img').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
                $(".model-body").append("<button type='button' class='upload-btn' id='upload-logo'><i class='icon-upload-cloud'></i> رفع الصورة</button>");
            }
            else
            {
                $(".model-msg").text("نوع الصورة الذي اخترته غير مقبول").fadeIn(400).delay(3000).fadeOut(400);
            }
        }
    });

    $(document).on("click","#upload-logo",function(){
        var fileData = $("#logo-input").prop("files")[0];
        var formData = new FormData();
        formData.append("file",fileData);
        $.ajax({
            url: "",
            type: "POST",
            data: formData,
            success: function(data){
                if(data == "")
                {
                    $(".image-form img").attr("src",$(".model-img").attr("src"));
                }
                else
                {

                }
            }
        });
    });

    $(document).on("click","#edit-contacts",function(){
        $.ajax({
            url: "./profile/edit/contacts",
            type: "POST",
            data: {
                m_mobile: $("#s-mobile").text()
                //phone: $("#s-phone").text()
            },
            success: function(data){
                if(data == "")
                {

                }
                else
                {
                  alert(data);
                }
            }
        });
    });

    $(document).on("click","#edit-address",function(){
        $.ajax({
            url: "",
            type: "POST",
            data: {
                address: $("#s-address").text(),
                nearby: $("#s-nearby").text()
            },
            success: function(data){
                if(data == "")
                {

                }
                else
                {

                }
            }
        });
    });

    $(document).on("click","#edit-identity",function(){
        $.ajax({
            url: "",
            type: "POST",
            data: {
                idNumber: $("#s-idno").text()
            },
            success: function(data){
                if(data == "")
                {

                }
                else
                {

                }
            }
        });
    });

    $(document).on("click","#edit-store",function(){
        $.ajax({
            url: "",
            type: "POST",
            data: {
                storeName: $("#s-storename").text()
            },
            success: function(data){
                if(data == "")
                {

                }
                else
                {

                }
            }
        });
    });

    $(document).on("click","#edit-passwd",function(){
        if($("#s-new-pwd").text()===""){
            alert("لازال الحقل خاليا, هل نسيت كتابة كلمة المرور ?!");
        }else if($("#s-new-pwd").text()!==$("#r-new-pwd").text()){
            alert("كلمتا المرور لا تتطابق");
        }else if($("#s-new-pwd").text().length < 8){
            alert("كلمة المرور الجديدة أقل من 8 احرف");
        }else{
            $.ajax({
                url: "./profile/cred",
                type: "POST",
                data: {
                    passwd: $("#s-new-pwd").text()
                },
                success: function(data){
                      alert(data);
                }
            });
        }
    });

    $(".product-model-close").click(function(){
        $(".popup-preview-img").fadeOut(600,function(){
            $(document).find(".delete-notice").remove();
            $(".delete-product-image").prop('disabled', false);
            $(".model-buttons").show();
        });
    });

    $(document).on("click",".product-overlay",function(){
        var imgSource = $(this).siblings("img").attr("src");
        $("#image-previewer").attr("src",imgSource);
        $(".popup-preview-img").css({"display":"flex"}).fadeIn(600);
    });

    $(".delete-product-image").click(function(){
        $(".model-buttons").hide();
        $(this).parent().after("<p class='delete-notice'>تريد حذف صورة هذا المنتج؟ <span class='delete-option'>حذف</span> <span class='undo-option'>إلغاء</span></p>");
        $(this).prop('disabled', true);
    });

    $(document).on("click",".delete-option",function(){
        var imageName = $("#image-previewer").attr("src"); 
        $.ajax({
            url: "",
            method: "POST",
            data: {imageName:imageName},
            success: function(data)
            {
                $(document).find(".delete-notice").empty().append("<p>تم حذف صورة المنتج</p>");
                setTimeout(function(){
                    $(".popup-preview-img").fadeOut(600,function(){
                        $(document).find(".delete-notice").remove();
                        $(".delete-product-image").prop('disabled', false);
                        $(".model-buttons").show();
                    });
                },2000);
            }
        });
    });

    $(document).on("click",".undo-option",function(){
        $(document).find(".delete-notice").remove();
        $(".delete-product-image").prop('disabled', false);
        $(".model-buttons").show();
    });

});