$(document).ready(function(){
    $(".control-input").focus(function(){
        if($(this).val().trim() == "")
        {
            $(this).siblings(".animated").css({
                "font-size" : "12px",
                "top" : "-16px",
                "background" : "#fff",
                "padding" : "2px 4px",
                "font-weight" : "bold"
            });
        }
    });

    $(".control-input").blur(function(){
        if($(this).val().trim() == "")
        {
            $(this).siblings(".animated").css({
                "font-size" : "16px",
                "top" : "7px",
                "background" : "transparent",
                "padding" : "0px",
                "font-weight" : "normal"
            });
        }
    });

    $(".ipass").click(function(){
        $(this).toggleClass("icon-eye-off").siblings(".control-input").attr("type",$(this).hasClass("icon-eye-off")?"text":"password");
    });
});