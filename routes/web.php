<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',  Livewire\Home::class)->name('home');
Route::get('/user-guide', Livewire\UserGuide::class)->name('kb');
Route::get('/blog', Livewire\Blog::class)->name('blog');
Route::get('/article1', Livewire\Article1::class)->name('/blog/1');
Route::get('/article2', Livewire\Article2::class)->name('/blog/2');
Route::get('/article3', Livewire\Article3::class)->name('/blog/3');
Route::get('/article4', Livewire\Article4::class)->name('/blog/4');
Route::get('/common-questions', Livewire\CommonQuestions::class)->name('faqs');
Route::get('/registration',  Livewire\Registration::class)->name('reg');
Route::get('/admin',  Livewire\Admin::class)->name('admin');